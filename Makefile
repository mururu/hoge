all: dep

compile:
	./rebar compile skip_deps=true

deps: get-deps update-deps
	./rebar compile

get-deps:
	./rebar get-deps

update-deps:
	./rebar update-deps

pkg: compile
	rm -rf rel/hoge
	(cd rel && ../rebar generate)
