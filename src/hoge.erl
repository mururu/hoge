-module(hoge).

-behaviour(gen_server).

-export([get/1, put/2]).

-export([start_link/0]).

-export([init/1,
         handle_call/3,
         handle_cast/2,
         handle_info/2,
         terminate/2,
         code_change/3]).


start_link() ->
    gen_server:start_link({local, ?MODULE}, ?MODULE, [], []).


init([]) ->
    %_Old = process_flag(trap_exit, true),
    PrivDir = code:priv_dir(hoge),
    Path = filename:join([PrivDir, "eleveldb." ++ atom_to_list(node())]),
    {ok, Ref} = eleveldb:open(Path, [{create_if_missing, true}]),
    {ok, Ref}.

get(Key) ->
    gen_server:call(?MODULE, {get, Key}).

put(Key, Value) ->
    gen_server:call(?MODULE, {put, Key, Value}).

handle_call({get, Key}, _From, Ref) ->
    Reply = case eleveldb:get(Ref, Key, []) of
                not_found ->
                    not_found;
                {ok, Value} ->
                    Value
            end,
    {reply, Reply, Ref};
handle_call({put, Key, Value}, _From, Ref) ->
    ok = eleveldb:put(Ref, Key, Value, []),
    {reply, ok, Ref}.

terminate(_Reason, Ref) ->
    ok = eleveldb:close(Ref),
    ok.

handle_cast(_Msg, State) ->
    {noreply, State}.

handle_info(_Info, State) ->
    {noreply, State}.

code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
